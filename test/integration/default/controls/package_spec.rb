# frozen_string_literal: true

control 'pass-package-install-pkg-installed' do
  title 'should be installed'

  describe package('pass') do
    it { should be_installed }
  end
end
