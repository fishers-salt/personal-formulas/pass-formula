# frozen_string_literal: true

control 'pass-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'pass-config-file-repo-kitchen-auser-cloned' do
  title 'should be cloned'

  describe directory('/home/auser/.kitchen-pass-store') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
  end
end

control 'pass-config-file-repo-dir-kitchen-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.kitchen-pass-store/FORMULA') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('content') { should include('name: TEMPLATE') }
  end
end

control 'pass-config-file-zsh-rc-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/rc_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'pass-config-file-pass-kitchen-auser-alias-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/rc_includes/kitchen-pass-alias.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') do
      should include('alias kitchen-pass=\'PASSWORD_STORE_DIR=' \
                     '"/home/auser/.kitchen-pass-store" pass\'')
    end
  end
end
