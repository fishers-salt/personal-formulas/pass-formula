# frozen_string_literal: true

control 'pass-package-clean-pkg-absent' do
  title 'should not be installed'

  describe package('pass') do
    it { should_not be_installed }
  end
end
