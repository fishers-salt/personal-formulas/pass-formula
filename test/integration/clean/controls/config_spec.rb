# frozen_string_literal: true

control 'pass-config-clean-pass-kitchen-auser-alias-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/rc_includes/kitchen-pass-alias.zsh') do
    it { should_not exist }
  end
end

control 'pass-config-clean-repo-kitchen-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.kitchen-pass-store') do
    it { should_not exist }
  end
end
