# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as pass with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('pass-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_pass', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

pass-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

pass-config-file-zsh-rc-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/rc_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - pass-config-file-user-{{ name }}-present

{%- set repos = user.get('pass_stores', {}) %}
{%- for store, repo in repos.items() %}
pass-config-file-repo-{{ store }}-{{ name }}-cloned:
  git.latest:
    - name: {{ repo.repo }}
    - target: {{ home }}/{{ repo.target }}
    - user: {{ name }}
    {%- if repo.repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to pass without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}
    - require:
      - pass-config-file-user-{{ name }}-present

pass-config-file-repo-dir-{{ store }}-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/{{ repo.target }}
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - recurse:
      - user
      - group
    - require:
      - pass-config-file-repo-{{ store }}-{{ name }}-cloned

{%- if repo.alias is defined %}
pass-config-file-pass-{{ store }}-{{ name }}-alias-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/rc_includes/{{ store }}-pass-alias.zsh
    - source: {{ files_switch([
                  name ~ '-pass-alias.zsh.tmpl',
                  'pass-alias.zsh.tmpl'],
                lookup='pass-config-file-pass-' ~ store ~ '-' ~ name ~ '-alias-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        alias: {{ repo.alias }}
        target: {{ home }}/{{ repo.target }}
    - require:
      - pass-config-file-zsh-rc-include-dir-{{ name }}-managed
{% endif %}
{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
